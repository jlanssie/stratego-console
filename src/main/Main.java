package main;

import java.util.Scanner;
import model.*;

public class Main {
    private static Board board;
    private static Scanner keyboard = new Scanner(System.in);
    private static Player player1;
    private static Player player2;

    public static void main(String[] args) {
        board = Board.getInstance();

        player1 = new ComputerPlayer(Color.RED, Player.PlaceOnBoard.TOP);
        player1.placePawns();

        player2 = initPlayer(Color.BLUE, Player.PlaceOnBoard.BOTTOM);
        player2.placePawns();

        System.out.println("Pawns are placed, this is the board: \n" + board.toString() + "\n");

        boolean hasWon = false;
        Player currentPlayer = player1;
        Player otherPlayer = getOtherPlayer(currentPlayer);

        do {
            System.out.println(currentPlayer.getName() + "'s turn.");
            currentPlayer.makeMove();
            currentPlayer.increaseScore();

            System.out.printf("\nScore of %s: %d%n", currentPlayer.getName(), currentPlayer.getScore());
            System.out.printf("Score of %s: %d%n", otherPlayer.getName(), otherPlayer.getScore() );

            hasWon = didUserWin(otherPlayer);
            if(hasWon) {
                System.out.printf("\n\nCongratulations %s, you have won the game!", currentPlayer.getName());
            } else {
                System.out.println("\nThis is the board: \n" + board.toString());
                otherPlayer = currentPlayer;
                currentPlayer = getOtherPlayer(currentPlayer);
            }
        } while(!hasWon);
    }

    private static Player getOtherPlayer(Player currentPlayer) {
        return currentPlayer == player1 ? player2 : player1;
    }

    public static Player getPlayerWithColor(Color color) {
        return player1.getColor() == color ? player1 : player2;
    }

    private static Player initPlayer(Color color, Player.PlaceOnBoard place) {
        System.out.print("Player: what's your name?\t");
        String name = keyboard.nextLine();
        return new Player(name, color, place);
    }

    private static boolean didUserWin(Player otherPlayer) {
        if(! otherPlayer.getArmy().canStillMove()) { return true; }
        if(otherPlayer.getArmy().isFlagCaptured()) { return true; }
        return  false;
    }

    public static void showRules() {
        System.out.println("Aan het begin van het spel staan een rood leger en een blauw leger tegenover elkaar; elk bestaand uit 40 stukken in verschillende rangen." +
                "De bedoeling is dat de vlag van de tegenstander wordt veroverd of dat de tegenstander niet meer kan bewegen. " +
                "Stukken mogen hoogstens 5 keer heen en weer bewegen; daarna moet de speler een andere zet doen.\n" +
                "Rood begint. Alle stukken mogen per beurt 1 veld horizontaal of verticaal op het bord bewegen. " +
                "Uitzonderingen zijn de verkenner, die zowel horizontaal als verticaal zo ver mag bewegen als mogelijk, en de vlag en de bommen die niet mogen bewegen. " +
                "Stukken kunnen niet over elkaar heen springen.\n" +
                "De stukken staan zodanig opgesteld dat de speler aan de andere kant van het bord niet kan zien waar welk stuk van de tegenstander staat. " +
                "Alleen als een stuk een ander stuk aanvalt – door op hetzelfde vak te stappen – wordt de rang van beide stukken bekendgemaakt. " +
                "Over het algemeen geldt dat het hoogste stuk wint, en het andere van het bord wordt gehaald. Als beide even hoog zijn, worden beide verwijderd.\n" +
                "Hierop zijn twee uitzonderingen:\n" +
                "- als de spion, die de laagste in rang is van alle stukken, het hoogste stuk aanvalt (de maarschalk), dan wordt de maarschalk juist wel verslagen. Valt de maarschalk echter de spion aan, dan verdwijnt de spion van het bord.\n" +
                "- een bom wordt alleen weggenomen als deze door een mineur wordt aangevallen.\n" +
                "De bommen en de vlag mogen na plaatsing niet meer verplaatst worden. Een bom wint van elk stuk dat het probeert te slaan, met uitzondering van de mineur. De vlag verliest van elke rang en het veroveren ervan is tevens het doel van het spel: de speler die als eerste de vlag van de tegenstander aantikt met een willekeurig rang, heeft gewonnen.");
    }
}
