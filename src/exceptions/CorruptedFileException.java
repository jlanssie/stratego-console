package exceptions;

public class CorruptedFileException extends Exception{
    public CorruptedFileException() { }
    public CorruptedFileException(String message) {
        super(message);
    }
}
