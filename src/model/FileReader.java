package model;

import exceptions.CorruptedFileException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class FileReader {
    private String filePath;

    public FileReader(String filePath) { this.filePath = filePath; }

    public ArrayList<int[]> readFile() throws FileNotFoundException, CorruptedFileException {
        File file = new File(filePath);
        Scanner scanner = new Scanner(file);
        ArrayList<int[]> rows = new ArrayList<>();
        while(scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] numbersText = line.split(":");
            try {
                int row = Integer.parseInt(numbersText[0]);
                int col = Integer.parseInt(numbersText[1]);
                int rank = Integer.parseInt(numbersText[2]);
                rows.add(new int[]{row, col, rank});
            } catch (NumberFormatException e) {
                throw new CorruptedFileException("The file contains incorrect characters");
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new CorruptedFileException("The file contains one or more rows with too little arguments");
            }
        }
        return rows;
    }
}
