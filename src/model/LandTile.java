package model;

import main.Main;

public class LandTile extends Tile {
    private Pawn pawn;

    public LandTile(int row, int col) {
        super(row, col);
    }

    public Pawn getPawn() {
        return pawn;
    }

    public void setPawn(Pawn pawn) {
        this.pawn = pawn;
    }

    public void removePawn() { pawn = null; }

    public boolean isEmtpy() { return pawn == null; }

    @Override
    public boolean isAccessible() {
        return true; //Any tile can be accessed, except water tiles
    }

    @Override
    public String toString() {
        if(pawn == null)
            return "    ";
        if(Main.getPlayerWithColor(pawn.getColor()) instanceof ComputerPlayer)
            return "  X ";
        else
            return String.format(" %2d ", pawn.getRank());
    }
}